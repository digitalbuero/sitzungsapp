"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from sitzungsapp.models import FormData, PostData


def test_form_data():
    """
    Tests 'FormData'
    """

    # Run function #1
    result1 = FormData([b"string1"], "string2")

    # Assert results #1
    assert result1.total_size == 7
    assert result1.search_terms == ["string2"]
    assert not result1.is_empty()

    # Run function #2
    result2 = FormData([b""])

    # Assert results #1
    assert result2.total_size == 0
    assert result2.search_terms == []
    assert result2.is_empty()


def test_post_data():
    """
    Tests 'FormData'
    """

    # Run function
    result = PostData("string")

    # Assert result
    assert not result.is_empty()
