"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

import pathlib

from fastapi.testclient import TestClient

# Global setup
# (1) Fixture directory
fixtures = pathlib.Path(__file__).parent.parent / "fixtures"


def test_index_get(test: TestClient):
    """
    Tests 'index' page (GET method)
    """

    # Run function
    response = test.get("/")

    # Assert result
    assert response.status_code == 200


def test_index_post(test: TestClient):
    """
    Tests 'index' page (POST method)
    """

    # Setup
    # (1) Files
    pdf_file = fixtures / "valid.pdf"

    # Run function
    response = test.post("/", files={"files": pdf_file.open("rb")})

    # Assert results
    assert response.status_code == 200
    assert "29 Termine als Download" in response.text


def test_index_query(test: TestClient):
    """
    Tests 'index' page (query)
    """

    # Setup
    # (1) Files
    pdf_file = fixtures / "valid.pdf"
    contents = pdf_file.open("rb")

    # Run function #1
    response = test.post("/", data={"query": b"milan"}, files={"files": contents})

    # Assert results #1
    assert response.status_code == 200
    assert "1 Termin als Download" in response.text

    # Run function #2
    response = test.post("/", data={"query": b"123"}, files={"files": contents})

    # Assert results #2
    assert response.status_code == 200
    assert "Für die Suche gibt es keine Ergebnisse!" in response.text


def test_index_invalid(test: TestClient):
    """
    Tests 'index' page (invalid)
    """

    # Setup
    # (1) Files
    json_file = fixtures / "invalid.json"
    pdf_file = fixtures / "invalid.pdf"

    # Run function #1
    response = test.post("/", files={"files": b""})

    # Assert result #1
    assert response.status_code == 400

    # Run function #2
    response = test.post("/", files={"files": pdf_file.open("rb")})

    # Assert result #2
    assert response.status_code == 413

    # Run function #3
    response = test.post("/", files={"files": json_file.open("rb")})

    # Assert result #3
    assert response.status_code == 415


def test_download(test: TestClient):
    """
    Tests 'download' page
    """

    # Run function
    response = test.post("/download/123", data={"base64": "TmljZSB0cnkgOkQ="})

    # Assert results
    assert response.status_code == 200
    assert response.text == "Nice try :D"
    assert (
        response.headers["content-disposition"]
        == 'attachment; filename="termine-123.ics"'
    )


def test_download_invalid(test: TestClient):
    """
    Tests 'download' page (invalid)
    """

    # Run function
    response = test.post("/download/123")

    # Assert result
    assert response.status_code == 404
