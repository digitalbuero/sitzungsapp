# Sitzungstermine as an App
[![License](https://badgen.net/badge/license/GPL/blue)](https://codeberg.org/S1SYPHOS/sitzungsapp/src/branch/main/LICENSE) [![Build](https://ci.codeberg.org/api/badges/S1SYPHOS/sitzungsapp/status.svg)](https://codeberg.org/S1SYPHOS/sitzungsapp/issues)

Eine Python-Webapplikation, mit der die wöchentlichen Sitzungsdienst-PDFs der Staatsanwaltschaften bequem in Kalenderdateien umgewandelt werden können.


## Über dieses Projekt

Dieses Projekt soll einen (kleinen) Beitrag dazu leisten, Platz zu schaffen für die eigentliche Arbeit der Vertreter:innen der Staatsanwaltschaft:

> **Beispiel:** StA Peter Müller erhält eine Email der Geschäftsstelle; im Anhang eine PDF-Datei mit den aktuellen Sitzungsterminen für die kommende Woche: eine vier Seiten lange Tabelle. Schließlich findet er, nach endlosem Scrollen, seine ersten fünf Termine. Uff, erstmal einen Kaffee ..

Die Strafstation meines Referendariats hieß für mich vor allem eines: endlich **Sitzungsdienst für die Staatsanwaltschaft** - den Verfahrensgang mitbestimmen dürfen, eigene Entscheidungen treffen - eine verantwortungsvolle und spannende Tätigkeit und Chance, für die ich sehr dankbar bin.

Aber schon bald ging es mir wie StA Peter Müller; und auch andere Referendar:innen waren von den zweimal wöchentlich erscheinenden PDF-Tabellen wenig begeistert:

> **Fortsetzung:** StA Peter Müller macht sich an die Arbeit - Straftaten ermitteln und Anklageschriften verfassen? Termine eintragen! Zeile für Zeile .. Sitzungssaal, Tag und Uhrzeit, Aktenzeichen .. fünfzehn Minuten copy&paste, dann weiter zum nächsten Sitzungstag - aber das ist bei den Kolleg:innen ja auch nicht anders ..

**Das Problem:** Je umfangreicher der Fall, desto mehr Vorbereitungszeit muss für dessen Bearbeitung eingeplant werden - denn für eine erstmalige umfassende rechtliche Würdigung des Sachverhalts ist nach dem Schluss der Hauptverhandlung (und damit vor dem eigenen Plädoyer) **keine Zeit** mehr.

**Die Lösung:** So entstand die Idee, Sitzungstermine aus den bereitgestellten Tabellen für die Verwendung in einem **digitalen Kalender** aufzubereiten. Die Anwendung sollte mit allen Sitzungslisten funktionieren, die mit dem Geschäftsstellenprogramm [web.sta](https://www.dvhaus.de/leistungen/web.sta) erstellt wurden.

> **Hinweis:** Die Geschäftsstellen haben darauf keinen Einfluss, weil die Verwaltungssoftware einen Kalenderexport schlicht nicht vorsieht.


## Über die Technik

Die Anwendung ist in der Programmiersprache [Python](https://de.wikipedia.org/wiki/Python_(Programmiersprache)) geschrieben und basiert auf dem Webframework [FastAPI](https://fastapi.tiangolo.com). Darauf aufbauend kommen das CSS-Framework [Windi CSS](https://windicss.org) sowie das JavaScript-Framework [Alpine.js](https://alpinejs.dev) zum Einsatz.

In einem ersten Schritt extrahiert die Anwendung alle Termine aus den zur Verfügung gestellten PDF-Dateien, ohne diese jedoch zu speichern - hierbei bedient sie sich der Programmbibliothek [`sitzungsdienst`](https://codeberg.org/S1SYPHOS/sitzungsdienst).

An dieser Stelle haben Nutzer:innen die Möglichkeit, Termine **nach Suchbegriffen zu filtern**, wie etwa dem eigenen Vor- oder Nachnamen - andernfalls werden alle Termine verwendet.

Anschließend wird eine Datei im sogenannten ICS-bzw. "iCalendar"-Format erstellt, die in **alle gängigen Kalenderprogramme** übernommen werden kann.

> **Hinweis:** Der gesamte Prozess findet *in-memory* statt, sodass auf dem Server keinerlei (personenbezogene) Daten gespeichert werden.


## Setup

Aktuell läuft die App auf einer [uberspace](https://uberspace.de)-Instanz: Dort werden [`gunicorn`](https://gunicorn.org) und mehrere [`uvicorn`](https://www.uvicorn.org)-Worker mittels `supervisor` in der nachfolgenden Konfiguration gestartet:

```ini
[program:fastapi]
directory=%(ENV_HOME)s/sitzungsapp
command=%(ENV_HOME)s/sitzungsapp/ENV/bin/gunicorn -c wsgi.py
autostart=true
autorestart=true
```

Happy coding!
