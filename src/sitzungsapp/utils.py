"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

import base64
import binascii
import hashlib
import pathlib
from typing import Any


def data2hash(data: Any) -> str:
    """
    Creates hash from data structure

    :param data: typing.Any Data structure
    :return: str Cryptographic digest over data
    """

    return hashlib.blake2b(str(data).encode("utf-8"), usedforsecurity=False).hexdigest()


# See https://stackoverflow.com/a/14996816
def human_size(data: bytes | int | float | str | pathlib.Path) -> str:
    """
    Provides human-readable filesize

    :param data: bytes | int | float | str | pathlib.Path
    :return: str
    """

    # Define number of bytes
    nbytes = 0

    # Detect whether input represents ..
    # (1) .. number of ..
    if isinstance(data, int):
        nbytes = data

    elif isinstance(data, float):
        nbytes = int(data)

    # (2) .. bytes ..
    elif isinstance(data, bytes):
        nbytes = len(data)

    # .. otherwise ..
    else:
        # .. attempt to ..
        try:
            # (1) .. create path object
            path = pathlib.Path(data)

            # (2) .. determine its filesize
            nbytes = path.stat().st_size

        # If something goes south ..
        except OSError:
            # .. attempt to ..
            try:
                # .. treat string as 'base64' first
                nbytes = len(base64.b64decode(data, validate=True))

            # .. otherwise ..
            except binascii.Error:
                # .. determine length of bytestring
                nbytes = len(bytes(data, "ascii"))

    # Define filesize abbreviations
    suffixes = ["B", "KB", "MB", "GB", "TB", "PB"]

    index = 0

    while nbytes >= 1024 and index < len(suffixes) - 1:
        nbytes /= 1024.0
        index += 1

    # Format size string
    size = f"{nbytes:.2f}".rstrip("0").rstrip(".")

    # Append filesize abbreviation
    return f"{size} {suffixes[index]}"
