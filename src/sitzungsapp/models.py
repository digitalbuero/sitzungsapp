"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi import File, Form
from markupsafe import escape
from pydantic.dataclasses import dataclass


@dataclass
class FormData:
    """
    Holds form data
    """

    # Uploaded files
    files: list[bytes] = File(...)

    # Search terms
    query: str = Form(None)

    @property
    def total_size(self) -> int:
        """
        Provides total size of files

        :return: int
        """

        return sum(len(file) for file in self.files)

    @property
    def search_terms(self) -> list[str]:
        """
        Provides search terms

        :return: list[str]
        """

        return [] if self.query is None else str(escape(self.query)).split(";")

    def is_empty(self) -> bool:
        """
        Checks whether files were uploaded

        :return: bool
        """

        return not [file for file in self.files if file]


@dataclass
class PostData:
    """
    Holds download data
    """

    # Base64-encoded data
    base64: str = Form(None)

    def is_empty(self) -> bool:
        """
        Checks whether base64-encoded data is present

        :return: bool
        """

        return self.base64 is None
