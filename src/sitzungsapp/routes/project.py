"""
This module is part of the 'sitzungsapp' package,
which is released under GPL-3.0-only license.
"""

from fastapi import APIRouter, Request

from ..templating import jinja2

# Initialize router
project = APIRouter(prefix="/ueber-das-projekt")


@project.get("/")
async def index(request: Request) -> jinja2.TemplateResponse:
    """
    Serves 'project' page

    :param request: Request
    :return: starlette.templating._TemplateResponse
    """

    return jinja2.TemplateResponse("pages/project.html", {"request": request})
