import Alpine from 'alpinejs'

// Import modules
import cursor from './components/cursor'
import dropdown from './components/dropdown'
import terminal from './components/terminal'
import upload from './components/upload'

// Add data objects
Alpine.data('cursor', cursor)
Alpine.data('dropdown', dropdown)
Alpine.data('terminal', terminal)
Alpine.data('upload', upload)

// Initialize `alpinejs`
Alpine.start()
