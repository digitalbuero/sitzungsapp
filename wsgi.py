from multiprocessing import cpu_count
from os.path import dirname

wsgi_app = "main:app"
bind = ":1024"
chdir = dirname(__file__)
workers = cpu_count() * 2 + 1
worker_class = "uvicorn.workers.UvicornWorker"
max_requests = 1024
